# Ansible-Demo



## Getting started

This is just repository that demonstrates one of the ansible features to install NGINX.

To run this playbook, execute the following command :

```
ansible-playbook playbook.yaml -i inventory.ini -l webservers
```

Of course `inventory.ini` in this repository is just random IP and names. Customize it as you need.